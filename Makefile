KUBERNETES_CONTEXT=kubernetes-admin@talex.k8s
SSH_PRIVATE_KEY=~/.ssh/id_rsa
ANSIBLE_INVENTORY=env/inventory

export KUBECONFIG=/Users/talex/.kube/kubernetes-admin-talex.k8s

use-context:
	kubectl config get-contexts
	kubectl config use-context $(KUBERNETES_CONTEXT)
	kubectl config current-context

# Hetzner Cloud Infrastructure

infrastructure-create:
	ansible-playbook infrastructure-create.yaml

infrastructure-destroy:
	ansible-playbook infrastructure-destroy.yaml

#
# FIRST STEP: Kubernetes Infrastructure
#

kubernetes-step-1: setup-nodes kubeconfig

setup-nodes:
	ansible-playbook k8s-setup-nodes.yaml -i $(ANSIBLE_INVENTORY) --private-key $(SSH_PRIVATE_KEY)

kubeconfig:
	ansible-playbook k8s-export-kubeconfig.yaml -i $(ANSIBLE_INVENTORY) --private-key $(SSH_PRIVATE_KEY)

#
# SECOND STEP: Deploy Applications
#
# update KUBECONFIG with downloaded kubeconfig, use pbcopy and configure tools like k8s Lens
# e.g. "export KUBECONFIG=~/.kube/kubernetes-admin-talex.k8s"

kubernetes-step-2: deploy-dns-helper deploy-monitoring deploy-ingress deploy-databases

deploy-dns-helper: use-context
	ansible-playbook k8s-deploy-dns-helper.yaml -i $(ANSIBLE_INVENTORY)

deploy-monitoring: use-context
	ansible-playbook k8s-deploy-monitoring.yaml -i $(ANSIBLE_INVENTORY)

deploy-ingress: use-context
	ansible-playbook k8s-deploy-ingress.yaml -i $(ANSIBLE_INVENTORY)

deploy-databases: use-context
	ansible-playbook k8s-deploy-databases.yaml -i $(ANSIBLE_INVENTORY)
