# Hetzner Cloud project API token
hetzner_api_token: <MY_TOKEN>
# Hetzner DNS Console API token
hetzner_dns_token: <MY_TOKEN>
hetzner_dns_zone: domain.org
# Public SSH keys to connect hosts
ssh_authorized_keys:
  - ssh-rsa <MY_SSH_KEY>
# Datacenter name (Nuremberg DC Park 1)
datacenter: nbg1
# Hetzner Cloud server types for cluster master node(s)
master_server_type: cx21
# Hetzner Cloud server types for cluster worker node(s)
worker_server_type: cpx31
# Number of worker nodes
worker_count: 3
# Domain for master, workers and lb. (rDNS, Hostname)
cluster_name: my-cluster.k8s
domain: my-cluster.k8s.domain.org
domain_filters:
  - domain.org
grafana_hostname: grafana-my-cluster
traefik_hostname: traefik-my-cluster
# Private network settings
network_name: kubernetes
network_ip_range: 10.0.0.0/8
subnet_master_ip_range: 10.0.1.0/24
subnet_master_ip: 10.0.1
subnet_worker_ip_range: 10.0.2.0/24
subnet_worker_ip: 10.0.2
# Kubernetes version
kubernetes_version: 1.20.11
# Kubernetes local context
local_kube_context_path: /Users/username/.kube
# Docker version
docker_version: 5:20.10.8~3-0~ubuntu-focal
# Hetzner Linux image
os_image: ubuntu-20.04
# kubeconfig
kubeconfig: ~/.kube/kubernetes-admin-my-cluster.k8s
# Kubernetes Namespaces
namespaces:
  externaldns: external-dns
  prometheusOperator: monitoring
  traefik: traefik
  postgres: postgres
  influxdb: influxdb
# Helm chart version pinning
helm_chart_version:
  externaldns: 5.4.11
  prometheusOperator: 16.14.1
  traefik: 10.1.1
  postgres: 10.5.3
  influxdb: 2.2.14
# Docker images version pinning
docker_image_version:
  hcloudCCM: v1.11.1
