provider "hetznerdns" {
  apitoken = var.dns_token
}

data "hetznerdns_zone" "cluster" {
  name = var.dns_zone
}

resource "hetznerdns_record" "master" {
  count   = 1
  zone_id = data.hetznerdns_zone.cluster.id
  name    = replace(hcloud_rdns.rdns_master[count.index].dns_ptr, ".${var.dns_zone}", "")
  value   = hcloud_rdns.rdns_master[count.index].ip_address
  type    = "A"
  ttl     = 1800
}

resource "hetznerdns_record" "worker" {
  count   = var.worker_count
  zone_id = data.hetznerdns_zone.cluster.id
  name    = replace(hcloud_rdns.rdns_worker[count.index].dns_ptr, ".${var.dns_zone}", "")
  value   = hcloud_rdns.rdns_worker[count.index].ip_address
  type    = "A"
  ttl     = 1800
}
